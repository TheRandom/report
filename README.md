# Report builder

This project aims to build a report from a markdown file by submitting the file and a form to a webapp.

## Requirements

On `Debian` :

- pandoc
- texlive-latex-recommended
- texlive-fonts-extra
- texlive-latex-extra
- p7zip-full

- `$ pip3 install flask[async]`


## website

All the code of the webapp is under `website/report.py`

## Docker

In the `docker` folder there is a Dockerfile used to build and run the app.

## Get started

`$ python3 website/report.py`

Connect on your browser on [http://localhost:5000](http://localhost:5000).

Complete form and submit.
