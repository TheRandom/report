# website/report.py
# Main app script

# File generation
import tempfile
import string
import random

import asyncio

import os
import subprocess
from flask import Flask, flash, \
        request, redirect, url_for, send_from_directory
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = '/uploads'
ALLOWED_EXTENSIONS = {'md'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

def build_report(filename, form_data) -> str:
    header = """---
title: "{}"
author: ["{}"]
date: "{}"
subject: "Markdown"
keywords: [Markdown, Example]
subtitle: "{}"
lang: "fr"
titlepage: true
titlepage-color: "1E90FF"
titlepage-text-color: "FFFAFA"
titlepage-rule-color: "FFFAFA"
titlepage-rule-height: 2
book: true
classoption: oneside
code-block-font-size: \scriptsize
---
    """.format(
            form_data["title"],
            form_data["author"],
            form_data["date"],
            form_data["subtitle"]
            )
    print("header:\n%s", header)
    file_data = None
    with open(filename, 'r') as f:
        file_data = f.read()
    with open(filename, 'w+') as f:
        f.write(header)
        f.write(file_data)

    command = """\
    /usr/bin/pandoc {} \
    -o /output/report.pdf \
    --from markdown+yaml_metadata_block+raw_html \
    --template eisvogel \
    --table-of-contents \
    --toc-depth 6 \
    --number-sections \
    --top-level-division=chapter \
    --highlight-style breezedark \
    --variable urlcolor=cyan \
    --resource-path=.:src
    """.format(filename)
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    return "report.pdf"


def allowed_file(filename) -> str:
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/", methods=['GET', 'POST'])
async def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        form_data = {}
        # FIXME: Check if form is complete
        form_data = dict(request.form)
        print("form_data: %s", form_data)
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            random_dir = tempfile.mkdtemp(prefix=get_random_string(12),\
                    dir=app.config['UPLOAD_FOLDER'])
            filename = "report" + get_random_string(12) + ".md"
            filename = os.path.join(random_dir, filename)
            file.save(filename)
            output_file = build_report(filename, form_data)
            print("output_file: %s", output_file)
            return redirect(url_for('download_file', name=output_file))

    # FIXME: Add more input parameters in form
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <p>Title: <input value="Report Title" type=text name=title></p><br/>
      <p>Author: <input value="John Doe" type=text name=author></p><br/>
      <p>Date: <input value="2000-01-01" type=text name=date></p><br/>
      <p>Subtitle: <input value="Subtitle" type=text name=subtitle></p><br/>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''
@app.route('/uploads/<name>')
def download_file(name):
    return send_from_directory("/output", name)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
