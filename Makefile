.PHONY: build run

build:
	cd docker && make build

run: build
	cd docker && make run
